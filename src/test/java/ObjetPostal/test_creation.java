package ObjetPostal;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

public class test_creation {
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	Lettre l1,l2;
	Colis c1,c1_fake,c2_void,c2;
	@BeforeEach
	public void setUp() {
		l1=new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		l2=new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		c1=new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		c2_void=new Colis ();
		c2 = new Colis("inconnue","inconnue","0000",0, 0,Recommandation.zero,"vide", 0);
	
	
	}
	
	

	@Test 
	public void getter() {
		 assertEquals(1024,c1.getPoids());
		 assertEquals("Le pere Noel",c1.getOrigine());
	}
	
	
	@Test 
	public void testEquals() {
		assertFalse(c1.equals(c2_void));
		assertTrue(c2.equals(c2_void));
		assertTrue(c2.equals(c2));
		assertFalse(c2.equals(null));
		assertFalse(c2.equals(l1));
		
	}

	@Test
	public void sac1() {
		SacPostal sac1 = new SacPostal();
		sac1.ajoute(l1);
		sac1.ajoute(l2);
		sac1.ajoute(c1);
		assertEquals(116.5,sac1.getValeurRemboursement(),tolerancePrix);
		assertEquals(0.025359999558422715,sac1.getVolume(),toleranceVolume);
		SacPostal sac2 = sac1.SacOfCodeObj("7877");
		
		assertEquals(0.02517999955569394,sac2.getVolume(),toleranceVolume);
		
	}
	
	
}
