package ObjetPostal;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LettreTest {
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	Lettre lettreReco1,lettreReco2,lettreDefault;


	@BeforeEach
	void setUp() throws Exception {
		
		lettreReco1=new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettreReco2=new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		lettreDefault=new Lettre ();
	}

	@AfterEach
	void tearDown() throws Exception {
	}
/*
	@Test
	void testGetTarifBase() {
		fail("Not yet implemented");
	}*/

	@Test
	public void testTarifRemboursement() {
		
		assertEquals(1.5,lettreReco1.tarifRemboursement(),tolerancePrix);
		assertEquals(15.0,lettreReco2.tarifRemboursement(),tolerancePrix);
		assertEquals(0,lettreDefault.tarifRemboursement(),tolerancePrix); 
	}


	@Test
	void testTarifAffranchissement() {
		assertEquals(2.3f,lettreReco2.tarifAffranchissement());
		assertEquals(1f,lettreReco1.tarifAffranchissement());
		
	}

	@Test
	public void testtostring() {
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire",lettreReco1.toString());
		assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence",lettreReco2.toString());
		
		
	}
	/*
	@Test
	void testTypeObjetPostal() {
		fail("Not yet implemented");
	}*/

	@Test
	void testLettre() {
		assertFalse(lettreDefault.isUrgence());
	}
	
	@Test 
	public void testGetTarifBase() {
		assertEquals(0.5f,lettreDefault.getTarifBase());
	}
	
	/*
	@Test
	void testLettreStringStringStringFloatFloatRecommandationBoolean() {
		fail("Not yet implemented");
	}

	@Test
	void testIsUrgence() {
		fail("Not yet implemented");
	}*/

}
