package ObjetPostal;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ObjetPostal.Colis;
import ObjetPostal.Lettre;
import ObjetPostal.Recommandation;

import ObjetPostal.ObjetPostal;
class ColisTest {
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	
	Colis c1,c2,c3,c22,c2_void;
	

	
	@BeforeEach
	public void setUp() throws Exception {

		c1=new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.un, "train electrique", 200);
		c2=new Colis("La mere Noel",
				"famille Kenya , igloo 20 , terre ouest",
				"5854",450,0.125f,Recommandation.zero,"train a vapeur ",300);
		c3=new Colis("La mere Noel",
				"famille Kaila , igloo 200 , terre sud",
				"9645",251,0.15f,Recommandation.deux,"train a charbon ",400);
		c2_void=new Colis ();
		c22 = new Colis("inconnue","inconnue","0000",0, 0,Recommandation.zero,"vide", 0);
	}
		
	
	@AfterEach
	void tearDown() throws Exception {
	}

	@Test 
	public void testGetTarifBase() {
		assertEquals(2,c2_void.getTarifBase());
	}

	@Test
	void testTarifRemboursement() {
		assertEquals(200,c3.tarifRemboursement(),tolerancePrix);
		assertEquals(0,c2.tarifRemboursement(),tolerancePrix);
		assertEquals(20,c1.tarifRemboursement(),tolerancePrix);
		
	}

	@Test
	public void testTarifAffranchissement() {
		assertEquals(6.5,c3.tarifAffranchissement(),tolerancePrix);
		assertNotEquals(5.0,c2.tarifAffranchissement(),tolerancePrix);
		assertEquals(2.5,c1.tarifAffranchissement(),tolerancePrix);
	
	}
	
	/*@Test
	void testToString() {
		fail("Not yet implemented");
	}

	@Test
	void testTypeObjetPostal() {
		fail("Not yet implemented");
	}*/

	@Test
	void testColis() {
		assertEquals(c22,c2_void);
	}

	@Test
	void testColisStringStringStringFloatFloatRecommandationStringFloat() {
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/1/0.02/200.0",c1.toString());
	}

	@Test
	void testGetDeclareContenu() {
		assertEquals("train electrique",c1.getDeclareContenu());
		
	}

	@Test
	void testGetValeurDeclaree() {
		assertEquals(200,c1.getValeurDeclaree());
		
	}

}
