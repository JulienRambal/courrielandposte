package Poste;

import java.io.File;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Assertions.*;

import Poste.Courriel;

/**
 * Unit test for simple App.
 */
public class MonJolieCourieldeTest {

	Courriel c1, c2, c3, c4, c5;

	@BeforeEach
	public void setUp() throws Exception {

		c1 = new Courriel("abc@gmail.com", "titre", "bonjour joint", new File("path\\lol\\something.txt"));
	
	}

	@Test
	public void testMailInvalide() {
	
		Assertions.assertThrows(Exception.class, () -> {
			
			new Courriel("abc@gmail.pom", "titre", "bonjour joint", new File("path\\lol\\something.txt"));
		});
		Assertions.assertThrows(Exception.class, () -> {
			new Courriel("abcgmail.com", "titre", "bonjour joint", new File("path\\lol\\something.txt"));
		});
		Assertions.assertThrows(Exception.class, () -> {
			new Courriel("@gmail.com", "titre", "bonjour joint", new File("path\\lol\\something.txt"));
		});
		Assertions.assertThrows(Exception.class, () -> {
			new Courriel("abc@gmailcom", "titre", "bonjour joint", new File("path\\lol\\something.txt"));
		});
		Assertions.assertThrows(Exception.class, () -> {
			new Courriel("abc@com", "titre", "bonjour joint", new File("path\\lol\\something.txt"));

		});
	}

	@Test
	public void testTitreInvalide() {

		Assertions.assertThrows(Exception.class, () -> {
			new Courriel("abc@gmail.com", "", "bonjour joint", new File("path\\lol\\something.txt"));
		});
	}
	@Test
	public void testPJInvalide() {

		Assertions.assertThrows(Exception.class, () -> {
			new Courriel("abc@gmail.com", "fezrfze", "bonjour joint", new File(""));
		});
		
	
	}
	
	

}
