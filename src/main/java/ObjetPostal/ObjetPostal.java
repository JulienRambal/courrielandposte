package ObjetPostal;

import java.util.Objects;

public abstract class ObjetPostal
{
	private String origine;
	private String destination;
	private String codePostal; // on choisit ici String pour ne pas se préoccuper des 0 non significatifs
	private float  poids;
	private float volume;
	private Recommandation    tauxRecommandation;

	// constructeurs
	
	protected String DefaultOrigine(){return  "inconnue";}
	protected String DefaultDestination(){return  "inconnue";}
	protected String DefaultCodePostal(){return  "0000";}
	protected float  DefaultPoids(){return  0;}
	protected float  DefaultVolume(){return  0;}
	protected Recommandation DefaultRecommandation(){return  Recommandation.zero;}
	
	public ObjetPostal()
	{
	origine=DefaultOrigine(); 
	destination=DefaultDestination(); 
	codePostal= DefaultCodePostal(); 
	poids=DefaultPoids(); volume=DefaultVolume(); 
	tauxRecommandation=DefaultRecommandation();
	}


	// comme la classe est abstraite, ne sera pas appelé à la suite d'un new mais dans les constructeurs des sous classes
	public ObjetPostal(String origine, String destination, String codePostal, float poids, float volume,
			Recommandation tauxRecommandation) {
		this.origine = origine;
		this.destination = destination;
		this.codePostal = codePostal;
		this.poids = poids;
		this.volume = volume;
		this.tauxRecommandation = tauxRecommandation;
	}



	//accesseurs, seuls certains accesseurs en lecture sont utiles dans notre exemple




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjetPostal other = (ObjetPostal) obj;
		return Objects.equals(codePostal, other.codePostal) && Objects.equals(destination, other.destination)
				&& Objects.equals(origine, other.origine)
				&& Float.floatToIntBits(poids) == Float.floatToIntBits(other.poids)
				&& tauxRecommandation == other.tauxRecommandation
				&& Float.floatToIntBits(volume) == Float.floatToIntBits(other.volume);
	}


	public String getOrigine()  {return origine;}
	//public void setOrigine(String o) {origine=o;}

	public String getDestination()  {return destination;}
	//public void setDestination(String d) {destination=d;}

	public String getCodePostal()   {return codePostal;}
	//public void setCodePostal(String cp) {codePostal=cp;}

	public float getPoids()   {return poids;}
	//public void setPoids(float p) {poids=p;}

	public float getVolume()   {
		return volume;}
	//public void setVolume(float v) {volume=v;}

	public Recommandation getTauxRecommandation()   {
		return tauxRecommandation;
	}  

	//public void  setTauxRecommandation(Recommandation txr) {   
	//tauxRecommandation=txr;
	//}  

	// tous les objets postaux ont un tarif de base, auquel on peut accéder grâce à la méthode getTarifBase
	public abstract  float getTarifBase();

	// autres méthodes

	abstract public float tarifRemboursement();
	
	protected float getTarifAffReco0(){return  0;}
	protected float getTarifAffReco1(){return 0.5f;}
	protected float getTarifAffReco2(){return  1.5f;}

	
	public float tarifAffranchissement()
	{// partie commune à tous les objets postaux pour le calcul du tarif d'affranchissement
		float t=getTarifBase(); //Lettre =0.5 ,Colis= 2 , class heritant  de ObjetPostal
		if (getTauxRecommandation()==Recommandation.un)  t=t+getTarifAffReco1(); //0.5 euros si le taux de recommandation est 1
		else if (getTauxRecommandation()==Recommandation.deux) t=t+getTarifAffReco2(); //1.5 euros si le taux de recommandation est 2
		else t=t+getTarifAffReco0();
		return t;
	}
	
	protected String delimiterToString() {return "/";}
	public String toString()
	{
		// partie commune dans le descriptif
		// ici on n'est pas obligé de passer par les accesseurs
		return typeObjetPostal()+" "+getCodePostal()+delimiterToString()+getDestination()+delimiterToString()+getTauxRecommandation();
		//Colis  code postal/destination/taux de recommandation 
		//Ex:
		//Colis 7854/famille Kaya, igloo 10, terres ouest/2
		
	}

	abstract public String typeObjetPostal(); // utile pour le toString, voir ci-dessus
	
	public void affiche() {
		System.out.println(toString());
	}
}