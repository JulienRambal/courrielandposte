package ObjetPostal;


public class Lettre extends ObjetPostal
{
	private boolean urgence;
	private static float tarifBase=0.5f; // 0.5 euro si le taux de recommandation est 1 , 1 par defaut
	private static String typeObjetPostal="Lettre";
	
	public Lettre()
	{ 
		// il est possible mais inutile d'ecrire super();
		urgence=false;
	}  



	public Lettre(String origine, String destination, String codePostal, float poids, float volume,
			Recommandation tauxRecommandation, boolean urgence) {
		super(origine, destination, codePostal, poids, volume, tauxRecommandation); // appel au constructeur de la super classe
		this.urgence = urgence;
	}



	//accesseurs

	public boolean isUrgence() {return urgence;}  
	//public void setUrgence(boolean u) {urgence=u;}

	//autres methodes

	public float getTarifBase(){return tarifBase;}
	
	protected float getTarifRemboursementReco0(){return  0;}
	protected float getTarifRemboursementReco1(){return 1.5f;}
	protected float getTarifRemboursementReco2(){return  15;}

	

	public float tarifRemboursement() {
	// 0 euro si le taux de recommandation est égal à 0,
	if (getTauxRecommandation()==Recommandation.un) return getTarifRemboursementReco1();//1.5 euros si le taux est égal à 1,
	else if (getTauxRecommandation()==Recommandation.deux) return getTarifRemboursementReco2(); //15 euros si le taux est égal à 2
	// else if (this.isUrgence())return 0.3 ; // 0.30 euro si c’est une lettre urgente.
	else return getTarifRemboursementReco0();
	}
	
	protected float getTarifAffDefault() {return 0;}
	protected float getTarifAffUrgent() {return 0.3f;}
	// redéfinition : spécialisation de la méthode tarifAffranchissement de la super classe
	public float tarifAffranchissement()
	{
		float t=super.tarifAffranchissement();
		if (isUrgence())
			t = t+getTarifAffUrgent(); //0.30 euro si c’est une lettre urgente.
		else t=t+getTarifAffDefault();
		return t;
	}

	protected String delimiterToString() {return "/";}
	protected String LettreUrgentToString() {return "urgence";}
	protected String LettreNotUrgentToString() {return "ordinaire";}
	
	public String toString()
	{
	String S = super.toString()+delimiterToString();
	if (isUrgence()) S += LettreUrgentToString();
	else S += LettreNotUrgentToString() ;
	return S;
	}

	public String typeObjetPostal() {return typeObjetPostal;}


}
