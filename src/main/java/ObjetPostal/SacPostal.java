package ObjetPostal;


import java.util.*;


public class SacPostal
{
	private ArrayList<ObjetPostal> contenu = new ArrayList<ObjetPostal>();
	private double capacite;

	protected double CapaciteDefault() {return 0.5;}
	public SacPostal()
	{this.capacite = CapaciteDefault();}

	public SacPostal(double _capacite)
	{ this.capacite = _capacite;}  

	//accesseurs

	public double getCapacite()
	{ return capacite;}

	//accesseur de consultation pour contenu possible
	//autres methodes
	public boolean ajoute(ObjetPostal objetToAdd)
	{
	if (objetToAdd.getVolume() + getVolume() <= capacite)
	{contenu.add(objetToAdd);
	System.out.println("l'objet :"+objetToAdd.toString()+"a ete ajouté ! ");
	return true;
	}
	else {
		System.out.println("l'objet :"+objetToAdd.toString()+"n'a pas ete ajouté car il manque de la place ");
		return false;
	}
	}
	
	protected double VolumeduSacVide() {return 0.005;}
	public double getVolume()
	{
	double Totalvolume=VolumeduSacVide();
	for (int i=0; i < contenu.size(); i++)
		Totalvolume += (contenu.get(i)).getVolume();
	return Totalvolume;
	}
	
	public double getValeurRemboursement()
	{
	double TotalduTarifdeRemboursement= 0;
	for (ObjetPostal objetduSac:contenu)
	{
		TotalduTarifdeRemboursement += objetduSac.tarifRemboursement();
	}
	return TotalduTarifdeRemboursement;
	}

	public String toString()
	{return "Sac \ncapacite: "+getCapacite()+
			"\nvolume: "+getVolume()+"\n"+contenu+"\n";}

		
	public SacPostal SacOfCodeObj(String codeDesObjetsAExtraire ) // pas vraiment de sens de faire param (String , SacPostal) 
	{
		// on calcule la capacite necessaire
		double capaciteNecessaire=0;
		for (ObjetPostal objetduSac:contenu)
		{
			System.out.println(objetduSac.toString());
			if (objetduSac.getCodePostal().equals(codeDesObjetsAExtraire))
				System.out.println("oui"+codeDesObjetsAExtraire);
		}
		capaciteNecessaire=contenu.stream().filter(objetduSac -> objetduSac.getCodePostal().equals(codeDesObjetsAExtraire)).count();
		SacPostal sacDesObjsExtrais = new SacPostal(capaciteNecessaire);
		ObjetPostal elementInSac ;
		for (int i=0;i<contenu.size();)
		{
		elementInSac  = contenu.get(i);
		if (elementInSac.getCodePostal().equals(codeDesObjetsAExtraire))
		{
		sacDesObjsExtrais.ajoute(elementInSac);
		contenu.remove(i);
		}
		else i++;
		}
		
		return sacDesObjsExtrais;
	}




}

