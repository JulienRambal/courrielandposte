package ObjetPostal;


import java.time.LocalDate;


public class ColisExpress extends Colis {
	private LocalDate dateEnvoi;
	private static int nbColisExpress=0; //on utilise pour cela un attribut statique nombreColisExpress
	private int numeroSuivi;
	private boolean emballagePoste; //muni de l’emballage proposé par la poste laponaise,*
	private static float affranchissementColisExpress=30f; // le tarif d’affranchissement est de 30 euros
	private static float tarifEmballage=3f; //*: dans ce cas le tarif d’affranchissement est augmenté de 3 euros.

	private static float poidsMax=30; //le poids doit être inférieur à 30kg

	public ColisExpress(String o, String d, String cP, float pds, float v, Recommandation t, String dC, float vD,
			boolean emballagePoste) throws ColisExpressInvalide {
		super(o, d, cP, pds, v, t, dC, vD); // appel du constructeur de la super classe Colis
		
		if (pds>=poidsMax) { // le poids doit être inférieur à 30kg
			throw new ColisExpressInvalide("poids incohérent, votre colis ne pourra pas être acheminé."); 
			}
		this.emballagePoste = emballagePoste;
		numeroSuivi=nbColisExpress; //on crée un numéro de suivi sur internet qui est défini au moment de la construction (on utilise pour cela un attribut statique nombreColisExpress qui augmente à chaque appel de constructeur),
		nbColisExpress++; // nombreColisExpress augmente à chaque appel de constructeur,
		dateEnvoi=LocalDate.now();//  on stocke la date d’envoi (date du moment de l’appel du constructeur)
	}

	// redéfinition en masquant le comportement hérité de la super-classe
	public float tarifAffranchissement() {
		float result=ColisExpress.affranchissementColisExpress;
		if (emballagePoste) result+=ColisExpress.tarifEmballage;
		return result;
	}

	public String typeObjetPostal() {return "Colis express";}
	public String toString() {
		return super.toString()+"/"+getPoids()+"/"+numeroSuivi; //poids/numéro de suivi
																//Colis Express 7855/famille Artick, igloo 90, baie des vents/2/0.02/2/20/20160128
																//Colis Express code postal/destination/taux de recommandation/volume/valeur déclarée/poids/numéro de suivi
	}

}
