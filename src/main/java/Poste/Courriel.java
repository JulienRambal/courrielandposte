package Poste;

import java.io.File;
import java.util.regex.*;

public class Courriel {

	public String mail;
	public String titre;
	public String corps;
	public File piece_jointe;

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) throws Exception {
		if (!titre.isEmpty())
			this.titre = titre;
		else {
			throw new Exception("Le titre est vide ");
		}
	}
	

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) throws Exception {
		verifyMail(mail);
		this.mail = mail;
	}
	
	private void verifyMail(String _mail) throws Exception {
		/*if(regexMail(this.mail).isEmpty()) {
			System.out.println("OK");
			throw new Exception("adresse mail incorrecte");
		}*/
		if(!regexMail(_mail)) {
			throw new Exception("adresse mail incorrecte");
		}
	}
	private static boolean regexMail(String entree) {
		Pattern p = Pattern.compile("^([a-zA-Z0-9]+)(\\.([a-zA-Z0-9]+))*(@([a-zA-Z0-9]+)(\\.(com|fr)))$");
		Matcher m = p.matcher(entree);
		return m.matches();
	}

	public String getCorps() {
		return corps;
	}

	public void setCorps(String corps) throws Exception {
		
	verifyCorps();
	this.corps = corps;

	}
	private void verifyCorps() throws Exception {
		if (verifyPJ(corps) && this.isEmptyPJ()) {
			throw new Exception("Il n'y a pas de piece jointe  ");
		}
	}
	
	public File getPiece_jointe() {
		return piece_jointe;
	}

	public void setPiece_jointe(File piece_jointe) {
		//System.out.println(piece_jointe.toString());
		this.piece_jointe = piece_jointe;
	}

	
	private static boolean verifyPJ(String _texte) {
		Pattern p = Pattern.compile(".*(PJ|jointe|joint).*");
		Matcher m = p.matcher(_texte);
		return m.matches();
	}
	
	private boolean isEmptyPJ() {
		return this.piece_jointe.equals(new File(""));
		
		
	}

	
	@Override
	public String toString() {
		return "Courriel [mail=" + mail + ", titre=" + titre + ", corps=" + corps + ", piece_jointe=" + piece_jointe
				+ "]";
	}

	public Courriel() {
		this.mail = new String();
		this.titre = new String();
		this.piece_jointe = new File("\\");
		this.corps = new String();

	}

	public Courriel(String _mail, String _titre, String _corps, File _piece_jointe) throws Exception {
		
		super();
		
		//mail
		if(regexMail(_mail))this.mail=_mail;
		else throw new Exception("adresse mail incorrecte");
		//titre
		if(!_titre.isEmpty())this.titre=_titre;
		else throw new Exception("le titre est vide ");
		//piece_jointe
		this.piece_jointe=_piece_jointe;
		//corps
		if(!(verifyPJ(_corps) && this.isEmptyPJ()))this.corps=_corps;
		else throw new Exception("le corps ne correspond pas au attente ");
		
		
		
	}

}
